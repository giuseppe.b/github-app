## Running the Project

First, install all dependencies, preferably using node 16:

```bash
nvm use 16
yarn
```

Then, run the project:

```bash
yarn dev
```

To run the tests:
```bash
yarn test
```

Storybook can be run with:
```bash
yarn storybook
```

## Project Decisions

### Folder Structure

Feature/module specific things are in their own folder inside /src/modules, each module only exports whatever needs to be used outside of the module, in this case only the pages.

### API/Requests

All requests are handled using React-Query, which is a library that handles caching, loading states, refetching, among other things, of external resources.

### Testing

All non-page components have unit tests for relevant functionality. Visual regression tests can be added using the storybook stories (not done here, but a common option is using Chromatic).

### Storybook

All components must have a storybook story, so we can have visual regression tests on all components. It's also easier to develop each component in isolation using Storybook.

