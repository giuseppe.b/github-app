import { useQuery, UseQueryOptions } from "react-query"

import {
  searchUsers,
  SearchUsersQueryVariables,
  SearchUsersQueryResponse,
  getUser,
  GetUserQueryVariables,
  GetUserQueryResponse,
  getUserRepos,
  GetUserReposQueryVariables,
  GetUserReposQueryResponse,
} from "../api/queries"

export const useSearchUsersQuery = (
  variables: SearchUsersQueryVariables,
  options?: UseQueryOptions<SearchUsersQueryResponse>
) => {
  return useQuery<SearchUsersQueryResponse>(
    ["SEARCH_USERS_QUERY", variables],
    () => searchUsers(variables),
    options
  )
}

export const useGetUserQuery = (
  variables: GetUserQueryVariables,
  options?: UseQueryOptions<GetUserQueryResponse>
) => {
  return useQuery<GetUserQueryResponse>(
    ["GET_USER_QUERY", variables],
    () => getUser(variables),
    options
  )
}

export const useGetUserReposQuery = (
  variables: GetUserReposQueryVariables,
  options?: UseQueryOptions<GetUserReposQueryResponse>
) => {
  return useQuery<GetUserReposQueryResponse>(
    ["GET_USER_REPOS_QUERY", variables],
    () => getUserRepos(variables),
    options
  )
}
