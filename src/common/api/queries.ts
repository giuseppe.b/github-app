import { Repository } from "@common/types/repository"
import { ListUser, UserProfile } from "@common/types/user"

//#region Search Users
export interface SearchUsersQueryVariables {
  search: string
  perPage?: number
}

export interface SearchUsersQueryResponse {
  total_count: number
  items: ListUser[]
}

export const searchUsers = async ({
  search,
  perPage = 10,
}: SearchUsersQueryVariables): Promise<SearchUsersQueryResponse> => {
  const query = new URLSearchParams()
  query.append("q", search)
  query.append("per_page", perPage?.toFixed(0))

  const response = await fetch(
    `https://api.github.com/search/users?${query.toString()}`,
    {
      method: "GET",
      headers: {
        Accept: "application/vnd.github.v3+json",
      },
    }
  )
  if (!response.ok) throw new Error(response.statusText)

  const data = response.json()

  return data
}
//#endregion

//#region Get User
export interface GetUserQueryVariables {
  username: string
}

export type GetUserQueryResponse = UserProfile

export const getUser = async ({
  username,
}: GetUserQueryVariables): Promise<GetUserQueryResponse> => {
  const response = await fetch(`https://api.github.com/users/${username}`, {
    method: "GET",
    headers: {
      Accept: "application/vnd.github.v3+json",
    },
  })

  if (!response.ok) throw new Error(response.statusText)
  const data = response.json()

  return data
}
//#endregion

//#region Get User Repos
export interface GetUserReposQueryVariables {
  username: string
}

export type GetUserReposQueryResponse = Repository[]

export const getUserRepos = async ({
  username,
}: GetUserReposQueryVariables): Promise<GetUserReposQueryResponse> => {
  const response = await fetch(
    `https://api.github.com/users/${username}/repos`,
    {
      method: "GET",
      headers: {
        Accept: "application/vnd.github.v3+json",
      },
    }
  )

  if (!response.ok) throw new Error(response.statusText)

  const data = response.json()

  return data
}
//#endregion
