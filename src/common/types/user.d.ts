export interface ListUser {
  login: string
  id: number
  avatar_url: string
}

export interface UserProfile {
  login: string
  id: number
  node_id: string
  avatar_url: string
  public_repos: number
  public_gists: number
  followers: number
  following: number
  created_at: Date
  updated_at: Date
  name: string
  company: string
  email: string
}
