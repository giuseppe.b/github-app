export interface Repository {
  name: string
  url: string
  stargazers_count: number
  language: string
  description: string
}
