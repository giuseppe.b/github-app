import { InputHTMLAttributes } from "react"

export const Input = (props: InputHTMLAttributes<HTMLInputElement>) => {
  return (
    <input
      css={{
        outline: "none",
        border: "1px solid #ccc",
        borderRadius: 6,
        padding: 8,
      }}
      {...props}
    />
  )
}
