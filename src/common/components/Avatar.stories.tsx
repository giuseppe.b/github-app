import { ComponentMeta } from "@storybook/react"

import { Avatar } from "./Avatar"

export default {
  title: "Components/Avatar",
  component: Avatar,
} as ComponentMeta<typeof Avatar>

export const Default = {
  args: {
    imageUrl: "https://avatars.githubusercontent.com/u/583231?v=4",
    size: 128,
  },
}
