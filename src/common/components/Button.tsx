import { ButtonHTMLAttributes, ReactNode } from "react"

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children: ReactNode
  onClick?: () => void
}

export const Button = (props: ButtonProps) => {
  return (
    <button
      css={{
        appearance: "none",
        backgroundColor: "#FAFBFC",
        border: "1px solid rgba(27, 31, 35, 0.15)",
        borderRadius: 6,
        padding: 8,
        cursor: "pointer",
        transition: "background-color 0.2s cubic-bezier(0.3, 0, 0.5, 1)",

        "&:hover": {
          backgroundColor: "#F3F4F6",
        },
      }}
      {...props}
    />
  )
}
