import userEvent from "@testing-library/user-event"

import { Input } from "./Input"

import { render, screen } from "@tests/utils"

describe("Input", () => {
  test("placeholer text is shown", async () => {
    // Arrange
    render(<Input placeholder="PLACEHOLDER_TEXT" />)

    // Assert
    expect(screen.getByPlaceholderText("PLACEHOLDER_TEXT")).toBeInTheDocument()
  })

  test("onChange is called when typing", async () => {
    // Arrange
    const onChange = jest.fn()
    render(<Input placeholder="PLACEHOLDER_TEXT" onChange={onChange} />)

    // Act
    const input = screen.getByPlaceholderText("PLACEHOLDER_TEXT")
    userEvent.paste(input, "VALUE")

    // Assert
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatchObject({
      target: { value: "VALUE" },
    })
  })
})
