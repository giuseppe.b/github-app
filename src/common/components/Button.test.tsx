import userEvent from "@testing-library/user-event"

import { Button } from "./Button"

import { render, screen } from "@tests/utils"

describe("Button", () => {
  test("onChange is called when clicking", async () => {
    // Arrange
    const onClick = jest.fn()
    render(<Button onClick={onClick}>LABEL</Button>)

    // Act
    const button = screen.getByText("LABEL")
    userEvent.click(button)

    // Assert
    expect(onClick).toHaveBeenCalledTimes(1)
  })
})
