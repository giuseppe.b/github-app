interface AvatarProps {
  imageUrl: string
  size?: number
}

export const Avatar = ({ imageUrl, size = 32 }: AvatarProps) => {
  return (
    <div
      css={{
        width: size,
        height: size,
        backgroundImage: `url(${imageUrl})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        borderRadius: "50%",
      }}
    />
  )
}
