import { ComponentMeta } from "@storybook/react"

import { RepositoryCard } from "./RepositoryCard"

export default {
  title: "Modules/Profile/Components/RepositoryCard",
  component: RepositoryCard,
} as ComponentMeta<typeof RepositoryCard>

export const Default = {
  args: {
    name: "Test Repository",
    description: "This is a test repository",
  },
}
