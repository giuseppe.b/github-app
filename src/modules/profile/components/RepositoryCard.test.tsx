import userEvent from "@testing-library/user-event"

import { RepositoryCard } from "./RepositoryCard"

import { render, screen } from "@tests/utils"

describe("RepositoryCard", () => {
  test("repo info text is shown", async () => {
    // Arrange
    render(
      <RepositoryCard
        name="Test Repo"
        description="Repo Description"
        language="Typescript"
        stargazers_count={20}
        url=""
      />
    )

    // Assert
    expect(screen.getByText("Test Repo")).toBeVisible()
    expect(screen.getByText("Repo Description")).toBeVisible()
  })
})
