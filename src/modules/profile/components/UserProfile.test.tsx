import userEvent from "@testing-library/user-event"

import { UserProfile } from "./UserProfile"

import { render, screen } from "@tests/utils"

describe("UserProfile", () => {
  test("user info is shown", async () => {
    // Arrange
    render(<UserProfile username="test_user" />)

    // Assert

    // find user name
    expect(await screen.findByText("Test User")).toBeVisible()

    // find repositories info
    expect(await screen.findByText("Repositories (1)")).toBeVisible()
    expect(await screen.findByText("Test Repo")).toBeVisible()
    expect(await screen.findByText("Repo Description")).toBeVisible()
  })
})
