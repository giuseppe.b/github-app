import { ComponentMeta } from "@storybook/react"

import { UserProfile } from "./UserProfile"

export default {
  title: "Modules/Profile/Components/UserProfile",
  component: UserProfile,
} as ComponentMeta<typeof UserProfile>

export const Default = {
  args: {
    username: "test_user",
  },
}
