import { useMemo } from "react"

import { RepositoryCard } from "./RepositoryCard"

import { Avatar } from "@common/components/Avatar"
import { useGetUserQuery, useGetUserReposQuery } from "@common/hooks/queries"

interface UserProfileProps {
  username: string
}

export const UserProfile = ({ username }: UserProfileProps) => {
  const { data, isLoading: userIsLoading } = useGetUserQuery(
    { username },
    { enabled: !!username }
  )
  const { data: reposData, isLoading: repoIsLoading } = useGetUserReposQuery(
    { username },
    { enabled: !!username }
  )

  const isLoading = useMemo(
    () => userIsLoading || repoIsLoading,
    [repoIsLoading, userIsLoading]
  )

  if (!data || !reposData || isLoading) return <div>Loading...</div>

  return (
    <div
      css={{
        display: "grid",
        gap: 16,
        gridTemplateColumns: "1fr 2fr",
        width: "100%",
      }}
    >
      <aside
        css={{
          display: "flex",
          flexDirection: "column",
          gap: 16,
          alignItems: "center",
        }}
      >
        <Avatar imageUrl={data.avatar_url} size={200}></Avatar>
        <span css={{ fontSize: 32, fontWeight: 600, textAlign: "center" }}>
          {data.name || data.login}
        </span>
      </aside>

      <section
        css={{
          display: "flex",
          flexDirection: "column",
          gap: 16,
        }}
      >
        <header css={{ fontSize: 24, fontWeight: 600 }}>
          Repositories ({data.public_repos})
        </header>
        <div
          css={{
            display: "flex",
            flexDirection: "column",
            gap: 8,
            maxHeight: 500,
            overflow: "auto",
          }}
        >
          {reposData.map((repo) => (
            <RepositoryCard key={repo.name} {...repo} />
          ))}
        </div>
      </section>
    </div>
  )
}
