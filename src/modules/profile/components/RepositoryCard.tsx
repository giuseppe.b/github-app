import { Repository } from "@common/types/repository"

export const RepositoryCard = ({ name, description }: Repository) => {
  return (
    <div
      css={{
        display: "flex",
        flexDirection: "column",
        gap: 8,
        backgroundColor: "#F3F3F3",
        padding: 8,
        borderRadius: 8,
      }}
    >
      <span
        css={{
          fontSize: 20,
          fontWeight: 500,
        }}
      >
        {name}
      </span>
      {description && (
        <span
          css={{
            fontSize: 16,
            fontWeight: 400,
            color: "#777",
          }}
        >
          {description}
        </span>
      )}
    </div>
  )
}
