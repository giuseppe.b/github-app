import { NextPage } from "next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"

import { UserProfile } from "../components/UserProfile"

import { Button } from "@common/components/Button"

export const UserProfilePage: NextPage = () => {
  const router = useRouter()
  const username = router.query.username as string

  return (
    <main
      css={{
        display: "flex",
        flexDirection: "column",
        gap: 32,
        width: "100%",
        maxWidth: 800,
        justifyContent: "center",
        alignItems: "start",
        paddingTop: 100,
        margin: "0 auto",
        paddingBottom: 24,
      }}
    >
      <Head>
        <title>Github App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Button onClick={() => router.back()}>Back</Button>
      <UserProfile username={username} />
    </main>
  )
}
