import { ComponentMeta } from "@storybook/react"

import { UserProfilePage } from "./UserProfilePage"

export default {
  title: "Modules/Profile/Pages/UserProfilePage",
  component: UserProfilePage,
} as ComponentMeta<typeof UserProfilePage>

export const Default = {}
