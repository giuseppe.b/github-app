import { NextPage } from "next"
import Head from "next/head"
import { useRouter } from "next/router"
import { useCallback } from "react"

import { UserSearch } from "../components/UserSearch"

export const SearchPage: NextPage = () => {
  const router = useRouter()
  const search = router.query.search as string

  const onSearchChange = useCallback(
    (value: string) => {
      router.push({
        pathname: "/",
        query: { search: value },
      })
    },
    [router]
  )

  return (
    <main
      css={{
        display: "flex",
        flexDirection: "column",
        gap: 16,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 100,
      }}
    >
      <Head>
        <title>Github App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header css={{ fontSize: 32, fontWeight: 700 }}>
        Github User Search
      </header>
      <UserSearch search={search} onSearchChange={onSearchChange} />
    </main>
  )
}
