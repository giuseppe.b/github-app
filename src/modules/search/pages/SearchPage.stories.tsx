import { ComponentMeta } from "@storybook/react"

import { SearchPage } from "./SearchPage"

export default {
  title: "Modules/Search/Pages/SearchPage",
  component: SearchPage,
} as ComponentMeta<typeof SearchPage>

export const Default = {}
