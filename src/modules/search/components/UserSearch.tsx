import Link from "next/link"
import { useRouter } from "next/router"
import { useState } from "react"

import { Avatar } from "@common/components/Avatar"
import { Button } from "@common/components/Button"
import { Input } from "@common/components/Input"
import { useSearchUsersQuery } from "@common/hooks/queries"

interface UserSearchProps {
  search?: string
  onSearchChange?: (search: string) => void
}

export const UserSearch = ({
  search = "",
  onSearchChange,
}: UserSearchProps) => {
  const [value, setValue] = useState(search)

  const { data, error } = useSearchUsersQuery({ search }, { enabled: !!search })

  return (
    <div
      css={{
        display: "flex",
        flexDirection: "column",
        gap: 16,
      }}
    >
      <form
        onSubmit={(event) => {
          event.preventDefault()
          onSearchChange?.(value)
        }}
        css={{ display: "flex", gap: 8 }}
      >
        <Input
          name="search"
          placeholder="Search for a user"
          value={value}
          onChange={(event) => setValue(event.target.value)}
        />
        <Button type="submit">Search</Button>
      </form>
      <div
        css={{
          display: "flex",
          flexDirection: "column",
          gap: 4,
        }}
      >
        {data?.items.map((user) => (
          <Link href={`/${user.login}`} key={user.id} passHref>
            <a
              css={{
                display: "flex",
                gap: 16,
                alignItems: "center",
                padding: 8,
                borderRadius: 6,
                textDecoration: "none",
                transition:
                  "background-color 0.2s cubic-bezier(0.3, 0, 0.5, 1)",
                ":hover": {
                  backgroundColor: "#eee",
                },
              }}
            >
              <Avatar imageUrl={user.avatar_url} size={32} />
              <span
                css={{
                  color: "#555",
                }}
              >
                {user.login}
              </span>
            </a>
          </Link>
        ))}
      </div>
      {error && <div css={{ color: "red" }}>{JSON.stringify(error)}</div>}
    </div>
  )
}
