import { ComponentMeta } from "@storybook/react"

import { UserSearch } from "./UserSearch"

export default {
  title: "Modules/Search/Components/UserSearch",
  component: UserSearch,
} as ComponentMeta<typeof UserSearch>

export const Default = {}
