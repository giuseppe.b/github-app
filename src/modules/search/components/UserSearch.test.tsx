import userEvent from "@testing-library/user-event"
import { rest } from "msw"

import { UserSearch } from "./UserSearch"

import { SearchUsersQueryResponse } from "@common/api/queries"
import { server } from "@tests/mswServer"
import { render, screen } from "@tests/utils"

describe("UserSearch", () => {
  beforeEach(() => {
    server.use(
      rest.get("https://api.github.com/search/users", (req, res, ctx) => {
        const response: SearchUsersQueryResponse = {
          total_count: 2,
          items: [
            {
              id: 1,
              login: "USER_1",
              avatar_url: "https://avatars1.githubusercontent.com/u/1?v=4",
            },
            {
              id: 2,
              login: "USER_2",
              avatar_url: "https://avatars1.githubusercontent.com/u/1?v=4",
            },
            {
              id: 3,
              login: "USER_3",
              avatar_url: "https://avatars1.githubusercontent.com/u/1?v=4",
            },
          ],
        }
        return res(ctx.json(response))
      })
    )
  })

  test("onSearchChange is called on search", async () => {
    // Arrange
    const onSearchChange = jest.fn()
    render(<UserSearch onSearchChange={onSearchChange} />)

    // Act
    userEvent.type(
      screen.getByPlaceholderText("Search for a user"),
      "test_user"
    )
    userEvent.click(screen.getByText("Search"))

    // Assert
    expect(onSearchChange).toHaveBeenCalledWith("test_user")
  })

  test("user search is performed when search query is available", async () => {
    // Arrange
    render(<UserSearch search="test_user" />)

    // Assert
    expect(await screen.findByText("USER_1")).toBeVisible()
    expect(await screen.findByText("USER_2")).toBeVisible()
    expect(await screen.findByText("USER_3")).toBeVisible()
  })
})
