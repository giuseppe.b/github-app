import { ReactNode, useRef } from "react"
import { HydrateProps, QueryClient, QueryClientProvider } from "react-query"
import { ReactQueryDevtools } from "react-query/devtools"
import { Hydrate } from "react-query/hydration"

export interface ReactQueryProviderProps {
  children: ReactNode
  state?: HydrateProps["state"]
}

const ReactQueryProvider = ({ children, state }: ReactQueryProviderProps) => {
  const queryClientRef = useRef<QueryClient>()
  if (!queryClientRef.current) {
    queryClientRef.current = new QueryClient()
  }

  return (
    <QueryClientProvider client={queryClientRef.current}>
      <Hydrate state={state}>
        {children}
        <ReactQueryDevtools initialIsOpen={false} />
      </Hydrate>
    </QueryClientProvider>
  )
}

export default ReactQueryProvider
