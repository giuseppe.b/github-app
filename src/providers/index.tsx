import type { AppProps } from "next/app"

import ReactQueryProvider from "./ReactQueryProvider"

const Providers = ({
  children,
  pageProps,
}: {
  children: React.ReactNode
  pageProps?: AppProps["pageProps"]
}) => {
  return (
    <ReactQueryProvider state={pageProps.dehydratedState}>
      {children}
    </ReactQueryProvider>
  )
}

export default Providers
