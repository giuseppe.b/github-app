import { initialize, mswDecorator } from 'msw-storybook-addon'
import { QueryClient, QueryClientProvider } from 'react-query'
import msw from '../tests/msw'

// Initialize MSW
initialize()

// Provide the MSW addon decorator globally
export const decorators = [
  (Story) => {
    const queryClient = new QueryClient()

    return (
      <QueryClientProvider client={queryClient}>
        <Story />
      </QueryClientProvider>
    )
  },
  mswDecorator,
]

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  msw,
}
