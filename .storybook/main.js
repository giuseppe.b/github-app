const path = require('path')

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions"
  ],
  "framework": "@storybook/react",
  webpackFinal: (config) => {
    config.module.rules[0].use[0].options.presets = [
      require.resolve("@emotion/babel-preset-css-prop")
    ];

    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve("babel-loader"),
      options: {
        presets: [require.resolve("@emotion/babel-preset-css-prop")]
      }
    });

    config.resolve.alias = {
      ...config.resolve.alias,
      "@common": path.resolve(__dirname, '../src/common'),
      "@modules": path.resolve(__dirname, '../src/modules'),
      "@test": path.resolve(__dirname, '../tests'),
    }

    return config
  }
}
