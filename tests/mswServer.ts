import { setupServer } from "msw/node"

import mswConfig from "./msw"

// This configures a request mocking server with the given request handlers.

export const server = setupServer(...mswConfig.handlers)
