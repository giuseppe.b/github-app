import profile from "./profile"
import search from "./search"
import userRepos from "./userRepos"

const msw = {
  handlers: [search, profile, userRepos],
}

export default msw
