import { rest } from "msw"

import { SearchUsersQueryResponse } from "../../src/common/api/queries"

export default rest.get(
  "https://api.github.com/search/users",
  (req, res, ctx) => {
    const response: SearchUsersQueryResponse = {
      total_count: 2,
      items: [
        {
          id: 1,
          login: "test",
          avatar_url: "https://avatars1.githubusercontent.com/u/1?v=4",
        },
        {
          id: 2,
          login: "test_user",
          avatar_url: "https://avatars1.githubusercontent.com/u/1?v=4",
        },
      ],
    }
    return res(ctx.json(response))
  }
)
