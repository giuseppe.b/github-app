import { rest } from "msw"

import { GetUserQueryResponse } from "../../src/common/api/queries"

export default rest.get(
  "https://api.github.com/users/:username",
  (req, res, ctx) => {
    const { username } = req.params

    const response: GetUserQueryResponse = {
      login: username as string,
      id: 1,
      node_id: "1",
      avatar_url: "https://avatars.githubusercontent.com/u/583231?v=4",
      company: "",
      name: "Test User",
      created_at: new Date("2020-01-01T00:00:00.000Z"),
      updated_at: new Date("2020-01-01T00:00:00.000Z"),
      email: "",
      followers: 0,
      following: 0,
      public_gists: 0,
      public_repos: 1,
    }
    return res(ctx.json(response))
  }
)
