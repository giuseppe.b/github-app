import { rest } from "msw"

import { GetUserReposQueryResponse } from "../../src/common/api/queries"

export default rest.get(
  "https://api.github.com/users/:username/repos",
  (req, res, ctx) => {
    const { username } = req.params

    const response: GetUserReposQueryResponse = [
      {
        description: "Repo Description",
        language: "Typescript",
        name: "Test Repo",
        stargazers_count: 200,
        url: "",
      },
    ]
    return res(ctx.json(response))
  }
)
